import streamlit as st
from TTS.api import TTS
import pydub
from pathlib import Path
from glob import glob
import uuid
import time as T

st.title("Voice Cloner")
def generate_audio_file(input_text: str, voice_exam_path: str, lang:str):
    tts = TTS(model_name="tts_models/multilingual/multi-dataset/your_tts", progress_bar=False, gpu=False)
    output_audio_file = "output_audio/" + str(uuid.uuid1()) + ".wav"
    tts.tts_to_file(input_text, speaker_wav=voice_exam_path, language=lang,
                    file_path=output_audio_file)
    return output_audio_file

def display_wavfile(wavpath):
    audio_bytes = open(wavpath, 'rb').read()
    file_type = Path(wavpath).suffix
    sample_rate = 44100
    st.audio(audio_bytes, format=f'audio/{file_type}', start_time=0)


tab1, tab2 = st.tabs(["Upload Audio", "Generate Speech"])
with tab1:
    uploaded_file = st.file_uploader("Upload record file (Only support wav", type=['wav'])
    if uploaded_file is not None:
        if uploaded_file.name.endswith('wav'):
            audio = pydub.AudioSegment.from_wav(uploaded_file)
            file_type = 'wav'
    if st.button('Submit'):
        save_path = Path('input_audio') / uploaded_file.name
        audio.export(save_path, format=file_type)

with tab2:
    vocal = st.selectbox("Please select audio vocal", sorted(glob('input_audio/*.wav')))
    lang = st.selectbox("Please select language", ["en", "fr-fr", "pt-br"])
    input_text = st.text_input(
        "Enter text to generate 👇")
    if st.button("Generate"):
        output_audio_path = generate_audio_file(input_text, vocal, lang)
        display_wavfile(output_audio_path)



